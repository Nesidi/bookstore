<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List of Books</title>

    <!-- Bootstrap CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {
            padding-top: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    {!! Form::open(['method' => 'GET',
                 'route' => ['admin-panel.create']]) !!}
    {!! Form::submit('CREATE', ['class' => 'btn btn-info']) !!}
    {!! Form::close() !!}
    CONTENT SEARCH
    <!-- таблица со списком книг -->
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>author</th>
            <th>book</th>
            <th>genre</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Edit</th>
        </tr>
        </thead>
    </table>
</div>

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<!-- App scripts -->
<script>
    var SITEURL = '{{URL::to('')}}';
    $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: SITEURL +'{!! '/bookstable' !!}',
            columns: [
                { data: 'author', name: 'author' },
                { data: 'book', name: 'book' },
                { data: 'genre', name: 'genre' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false},

            ]
        });
    });
</script>


<!-- <a href="search"> SEARCH </a> -->

</body>
</html>