<!-- ссылка на редактирование книги и форма для удаления книги в столбец таблицы datatables со списком книг -->
<div class="">
    <a href="/admin-panel/{{ $id }}/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
    {!! Form::open(['method' => 'DELETE',
                'route' => ['admin-panel.destroy', $id ]]) !!}
    <div class="form-group">
        {{ Form::submit('Del') }}
    </div>
    {!! Form::close() !!}
</div>


