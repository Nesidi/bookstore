@extends('Admin.admin-index')

@section('title', 'Список книг')

@section('content')
    <form action="/search5" method="POST" role="search">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="q"
                   placeholder="Search books">
            <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
        </div>
    </form>

    <br>
    {!! Form::open(['method' => 'GET',
                 'route' => ['admin-panel.create']]) !!}
    {!! Form::submit('CREATE', ['class' => 'btn btn-info']) !!}
    {!! Form::close() !!}
    <br>
    <table class="table table-striped">
        <thead class="thead-dark">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Author</th>
            <th scope="col">Book</th>
            <th scope="col">Genre</th>
            <th scope="col">Date</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($book as $b)
            <tr>
                <th scope="row">{{$b->id}}</th>
                <td>{{$b->author}}</td>
                <td>{{$b->book}}</td>
                <td>{{$b->genre}}</td>
                <td>{{$b->created_at->format('d.m.Y') }}</td>
                <td><a href="{{URL::to('admin-panel/'. $b->id) . '/edit'}}"> Edit </a></td>
                <td>{!! Form::open(['method' => 'DELETE',
                 'route' => ['admin-panel.destroy', $b->id]]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!} </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    {{$book->links()}}

@endsection
