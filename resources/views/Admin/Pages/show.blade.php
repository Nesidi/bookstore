@extends('admin.admin-index')

@section('title', 'Список')

@section('content')
   <h1>{{$book->author}} - {{$book->book}} - {{$book->genre}} </h1> {{ Carbon\Carbon::parse($book->created_at)->format('d-m-Y') }}
   <br>
   <br>
   <button class="btn btn-default"> Изменить</button>

@endsection