@extends('admin.admin-index')

@section('title', 'Добавить')

@section('content')
    {!! Form::open(['route'=>'admin-panel.store'])!!}
    <div class="form-group">
        {{ Form::label('author','Автор') }}
        {{ Form::text('author',null) }}
            </div>
    <div class="form-group">
        {{ Form::label('book','Книга') }}
        {{ Form::text('book',null) }}
        </div>
    <div class="form-group">
        {{ Form::label('genre','Жанр') }}
        {{ Form::text('genre',null) }}
    </div>
    <div class="form-group">
        {{ Form::submit('Добавить') }}
    </div>
    {!! Form::close() !!}
    @endsection

