@extends('admin.admin-index')

@section('title', 'Добавить')

@section('content')
    {!! Form::model($book,array('route'=>array('admin-panel.update', $book->id), 'method'=>'PUT')) !!}
    <div class="form-group">
        {{ Form::label('author','Автор') }}
        {{ Form::text('author',null) }}
            </div>
    <div class="form-group">
        {{ Form::label('book','Книга') }}
        {{ Form::text('book',null) }}
        </div>
    <div class="form-group">
        {{ Form::label('genre','Жанр') }}
        {{ Form::text('genre',null) }}
    </div>
    <div class="form-group">
        {{ Form::submit('Изменить') }}
    </div>
    {!! Form::close() !!}
    @endsection

