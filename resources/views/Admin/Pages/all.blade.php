<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<ul>@foreach ($books as $book)
        <li><h2>{{$book->author}} - {{$book->book}} - {{$book->genre}} - {{ Carbon\Carbon::parse($book->created_at)->format('d-m-Y') }} -
        <a href="{{URL::to('admin-panel/'. $book->id) . '/edit'}}"> Изменить </a>
                {!! Form::open(['method' => 'DELETE',
                 'route' => ['admin-panel.destroy', $book->id]]) !!}
                {!! Form::submit('Удалить', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </h2> </li>
    @endforeach
</ul>
</body>
</html>