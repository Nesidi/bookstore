@extends('Admin.admin-index')

@section('title', 'Список книг')

@section('content')
    <form action="/search5" method="POST" role="search">
    {{ csrf_field() }}
    <div class="input-group">
        <input type="text" class="form-control" name="q"
               placeholder="Search books"> <span class="input-group-btn">
            <button type="submit" class="btn btn-primary" >
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>

<div class="container">
    @if(isset($details))
        <p> The Search results for " <b> {{ $query }} </b> " are :</p>
        <h2>TABLE</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Author</th>
                <th>Book</th>
                <th>Genre</th>
            </tr>
            </thead>
            <tbody>
            @foreach($details as $res)
                <tr>
                    <td>{{$res->author}}</td>
                    <td>{{$res->book}}</td>
                    <td>{{$res->genre}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>

@endsection