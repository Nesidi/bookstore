<?php

use App\Jobs\SendEmailJob;
use App\Books;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('admin-panel', 'Addbooks');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', function () {
    return view('users');
});


Route::get('/search4', 'AddBooks@index');

Route::any('/search5',function(){
    $q = Input::get ( 'q' );
    $res = Books::where('author','LIKE','%'.$q.'%')->orWhere('book','LIKE','%'.$q.'%')->orWhere('genre','LIKE','%'.$q.'%')->get();
    if(count($res) > 0)
        return view('find')->withDetails($res)->withQuery ( $q );
    else return view ('find')->withMessage('No Details found. Try to search again !');
});

Route::any('/search6',function(){
    return view('find');
});

Route::get('/test', function () {
    return view('test');
});
//Вызов контроллера Datatables
Route::get('/bookstable', 'BookSearch@getBasicData');

Route::get('/search', function () {
    return view('search');
})->middleware('auth');


Route::get('sendEmail', function(){

    SendEmailJob::dispatch()
        ->delay(now()->addSeconds(20));
    return 'EMAIL WAS SENT';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
