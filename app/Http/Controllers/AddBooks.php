<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Books;
use App;
use App\Events\onAddBook;

class AddBooks extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //обычный вывод списка книг ввиде таблицы (без datatables)
    public function index()
    {
        $book=App\Book::orderby('created_at','asc')->paginate(10);
        return view('admin.pages.index',compact('book'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // форма создания книги
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // добавление книги в БД
    public function store(Request $request)
    {
        $request->validate([
            'author' => 'required',
            'book' => 'required',
            'genre' => 'required',
        ]);
        $book=new Books();
        $book->author = $request->author;
        $book->book = $request->book;
        $book->genre = $request->genre;

        $book->save();
        $request->session()->flash('success', 'Добавлено!');


        $users = \App\User::all();

        foreach ($users as $user) {
            event(new onAddBook($book, $user->email));
//                ->delay(now()->addSeconds(5));
        }

        return redirect('search');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book=Books::find($id);
        return view('admin.pages.show')->withBook($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book=Books::find($id);
        return view('admin.pages.edit')->withBook($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // редактирование книги
    public function update(Request $request, $id)
    {
        $request->validate([
            'author' => 'required',
            'book' => 'required',
            'genre' => 'required',
        ]);
        $book=Books::find($id);
        $book->author = $request->author;
        $book->book = $request->book;
        $book->genre = $request->genre;

        $book->save();
        $request->session()->flash('success', 'Изменено!');

        return redirect('search4');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //удаление книги и возврат к таблице
    public function destroy($id)
    {
        $book=Books::find($id);
        $book->delete();
        return redirect()->back();
    }
}
