<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Yajra\DataTables\Facades\Datatables;
use Yajra\Datatables\Datatables;
use App\Books;
//use App;

class BookSearch extends Controller
{
    public function index()
    {

        return view('search');
    }

    public function getBasic()
    {
        return view('datatables.eloquent.basic');
    }
// использование Datatables для вывода таблицы книг из БД
    public function getBasicData()
    {
        $books = Books::select(['author', 'book', 'genre', 'created_at', 'updated_at', 'id']);

        return Datatables::of($books)
//            ->addColumn('action', function ($books) {
//                return '<a href="/admin-panel/'. $books->id. '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
//                <form action="/admin-panel/'. $books->id. '" method="POST" class="form-inline"><input type="submit" value="DELETE"/></form>';
//            })
                // добавление столбца edit delete в таблицу
            ->addColumn('action', function ($books) {
                return view('Admin.part.action', ['id' => $books->id]);
            })


            ->editColumn('created_at', function ($books) {
                return $books->created_at->format('d.m.Y');

            })
            ->editColumn('updated_at', function ($books) {
                return $books->updated_at->format('d.m.Y');

            })
            ->make(true);
    }

}

//namespace App\Http\Controllers;
//
//use App\Http\Requests;
//use App\Books;
//use Yajra\Datatables\Datatables;
//
//class DatatablesController extends Controller
//{
//    /**
//     * Displays datatables front end view
//     *
//     * @return \Illuminate\View\View
//     */
//    public function getIndex()
//    {
//        return view('search');
//    }
//
//    /**
//     * Process datatables ajax request.
//     *
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function anyData()
//    {
//        return Datatables::of(Books::query())->make(true);
//    }
//}
