<?php

namespace App\Listeners;

use App\Events\onAddBook;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\SendEmailMailable;
use Illuminate\Support\Facades\Mail;

class AddBookListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onAddBook $event
     * @return void
     */
    public function handle(onAddBook $event)
    {
        Mail::to($event->email)->send(new SendEmailMailable($event->book));

    }
}
