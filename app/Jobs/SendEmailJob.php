<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Mail\SendEmailMailable;
use Illuminate\Support\Facades\Mail;
use App\Books;
class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $book;
    public $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Books $Addedbook, $email)
    {
        $this->book = $Addedbook;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $users=DB::table('users')->get();
//        foreach ($users as $user) {
//
//            Mail::to($user->email)->send(new SendEmailMailable($this->book));
//        }
        Mail::to($this->email)->send(new SendEmailMailable($this->book));
    }
}
